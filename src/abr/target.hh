#ifndef TARGET_HH
#define TARGET_HH

#include "abr_algo.hh"
#include "./structs/estimators.hh"

class Target : public ABRAlgo {
    public:
        Target(const WebSocketClient & client, const std::string & abr_name, const YAML::Node & abr_config);

        VideoFormat select_video_format() override;

        void video_chunk_acked(Chunk && c) override;

        double localQoe(int current_ssim, int last_ssim, int rebuffer, int buffer); 

    private:
    bool VERBOSE = false;
    void adjustCC();
    std::unique_ptr<structs::MovingAverage<double>> bw_estimator;  
    int bandwidth_target;
    int last_adjustment_bandwidth;

    protected:
    std::pair<double, int> qoe(const double bandwidth, int quality, bool verbose);
    int curr_quality;
};

#endif
