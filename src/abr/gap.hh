#ifndef GAP_HH
#define GAP_HH

#include "target.hh"

class GapAbr : public Target {
    public:
        GapAbr(const WebSocketClient & client, const std::string & abr_name, const YAML::Node & abr_config);

        VideoFormat select_video_format() override;

    private:

    bool VERBOSE = true;
    uint32_t prev_cwnd[5];
    double avg_cwnd();
    void update_cwnd(uint32_t cwnd);

};




#endif