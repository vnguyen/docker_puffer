#include "gap.hh"
#include "ws_client.hh"

#include <iostream>


namespace GapAbrConstants {
  // horizon for adjustment for bandwidth
  const int horizon_adjustment = 5;
  
  // constants for optimization objective
  const double qoe_percentile = .95;
  const double qoe_delta = .20;
  const int step = 1000;

  const double over_percent = 1.25;

  // constants for deciding quality
  // const double safe_downscale = .8;
  const double safe_downscale = .9;
  const double endgame_safe_downscale = .7;
}

namespace {
  const int SECOND = 1000; 
}

VideoFormat GapAbr::select_video_format() {
    if (VERBOSE) std::cerr << "---------------- Gap ABR ----------------" << std::endl;
    const auto & channel = client_.channel();
    const auto & vformats = channel->vformats();
    size_t vformats_cnt = vformats.size();

    // handle std::optional
    uint64_t delivery_rate = 256; // can't be zero due to division
    uint32_t cwnd = 0;
    if (client_.tcp_info()) {
        delivery_rate = client_.tcp_info().value().delivery_rate;
        cwnd = client_.tcp_info().value().cwnd;
    }

    int bandwidth = 8 * delivery_rate / 100;

    //std::cerr << "GapAbr skeleton. Sending lowest quality format." << std::endl;

    // Get search range for bandwidth target
    int min_bw = int(bandwidth * (1. - GapAbrConstants::qoe_delta));
    int max_bw = int(bandwidth * (1. + GapAbrConstants::qoe_delta));
    

    int last_index = previous_vts; 
    int current_quality = curr_quality;
    int start_buffer = (int)(client_.video_playback_buf() * 100);
    uint64_t next_index = 0;
    double current_ssim = 0.0;
    double next_ssim = 0.0;
    size_t current_size = 0.0;

    unsigned int interval = channel->vduration();

    if (auto temp = client_.next_vts(); temp) { 
        next_index = temp.value();
    }

    if (client_.curr_vformat() && last_index != 0) {
        current_ssim = channel->vssim(client_.curr_vformat().value(), last_index);
        current_size = std::get<1>(channel->vdata(client_.curr_vformat().value(), last_index));
    }

    if (client_.curr_vformat() && next_index != 0) {
        next_ssim = channel->vssim(client_.curr_vformat().value(), next_index);
    }

    int segment_length_ms = (int)(((double)channel->vduration() / channel->timescale()) * 100.0);

    /*
    Data info:
    ssim range: [0, 1], but in reality [0.88,1]
    ssimdb range: in reality [8, 17]
    quality: [0,1..9]
    start_buffer range: [0, 1500], assuming max. buffer is 15s (setting in ws_client.hh)
    interval: segment ts length, should be constant per channel.
    size: kB
    bandwidth: kbps

    Note that the following console output refers to the previous cycle
    */

    if (false) {
        std::cerr << "Previous video send ts: " << last_index << std::endl
                << "Previous ssim: " << current_ssim << " SSIMdb: " << ssim_db(current_ssim) << std::endl
                << "Buffer: " << start_buffer << std::endl
                << "Next video send ts: " << next_index << std::endl
                << "Next ssim: " << next_ssim << " SSIMdb: " << ssim_db(next_ssim) << std::endl
                << "Segment duration: " << (double)channel->vduration() / channel->timescale() << std::endl
                << "Current size: " << current_size << std::endl
                << "Bandwidth: " << bandwidth << std::endl;
    }

    /* No need to log since that is done by Target qoe function */
    //logQuality(std::to_string(client_.connection_id()), last_index, ssim_db(current_ssim), (double)start_buffer);

    // if we are not looking at biggest quality
    double final_qoe_percentile = GapAbrConstants::qoe_percentile;
    if (current_quality < (int)(vformats_cnt - 1)) {
        // find the index for the smallest index difference
        int index_lowest_difference = last_index + interval;
        int current_difference = ssim_db(channel->vssim(vformats[current_quality + 1], last_index + interval))
                               - ssim_db(channel->vssim(vformats[current_quality], last_index + interval));
        
        for (int i = last_index + 2 * interval; i <= last_index + GapAbrConstants::horizon_adjustment * interval; i+=interval) {
            int ssim_diff = ssim_db(channel->vssim(vformats[current_quality + 1], i))
                          - ssim_db(channel->vssim(vformats[current_quality], i));
            if (ssim_diff < current_difference) {
                current_difference = ssim_diff;
                index_lowest_difference = i;
            }
        }

        // find the average segment size for the next horizion pieces after the planned transition
        int total_size = 0;
        int total_length = 0;
        for (int i = index_lowest_difference; 
                i < index_lowest_difference + GapAbrConstants::horizon_adjustment * interval; i+=interval) {
            total_size += std::get<1>(channel->vdata(vformats[current_quality + 1], i));
            total_length += segment_length_ms;
        }
        int avg_needed_bw = 8 * total_size / total_length;
    
        int max_bw_suggestion = avg_needed_bw * GapAbrConstants::over_percent; 
        float gain = 1. * (max_bw_suggestion * GapAbrConstants::safe_downscale - min_bw) / min_bw;
    
        // If it looks worthed to be more aggressive and the gain in bandwidth is attainable
        if (max_bw_suggestion > std::max(min_bw, max_bw)) {
            max_bw = std::min(max_bw_suggestion, max_bw * 2);

            // adjust percentile w.r.t. gain
            if (gain < .7) {
                final_qoe_percentile = .99;
            } else if (gain < 1.25) {
                final_qoe_percentile = .95;
            } else {
                final_qoe_percentile = .9;
            }

            // adjust percentile w.r.t. buffer, that is if the buffer is big, then we might
            // try to be more aggressive since we have not much to lose
            if (start_buffer > 8 * ::SECOND) {
                if (final_qoe_percentile == .95) {
                final_qoe_percentile = .99;
                } else if (final_qoe_percentile == .9) {
                final_qoe_percentile = .95;
                }
            }

            std::cerr << "[GapAbr] gain: "  << gain << std::endl;
            std::cerr << "[GapAbr] percentile: "  << final_qoe_percentile << std::endl;
        }
    }

    /*
    // Compute new bandwidth target -- this function should be strictly increasing 
    // as with extra bandwidth we can take the exact same choices as we had before
    bandwidth_target = max_bw;
    int qoe_max_bw = qoe(max_bw, current_quality, false).first; 
    int step = GapAbrConstants::step;
    while (
        bandwidth_target - step >= min_bw && 
        qoe(bandwidth_target - step, current_quality, false).first >= final_qoe_percentile * qoe_max_bw
    ) {
        bandwidth_target -= step;
    }

    std::cerr << "[GapAbr] bandwidth interval: [" << min_bw << ", " << max_bw << "]" << std::endl;
    std::cerr << "[GapAbr] bandwidth current: " << bandwidth << std::endl;
    std::cerr << "[GapAbr] bandwidth target: " << bandwidth_target << std::endl;

    // Adjust target rate -- if the bandwidth estimate decreases, don't force further decrease,
    // that is, use std::max(bandwidth, bandwidth_target)
    
    interface->setTargetRate(std::max(bandwidth, bandwidth_target)); 
    gap_interface->setTargetRate(std::max(bandwidth, bandwidth_target)); 
    */

    double safe_downscale = GapAbrConstants::safe_downscale;

    /*
    // Congestion detection
    if (gap_interface->recovery()) {
        safe_downscale = GapAbrConstants::endgame_safe_downscale;
    }*/

    bool congested = false;
    double two_average = ((double) cwnd + (double) prev_cwnd[4]) / 2.0;
    if (two_average < 0.4 * avg_cwnd() && two_average < 100) {
        std::cerr << "Congeston warning!!!";
        congested = true;
    } 
    std::cerr << "Congestion window: " << cwnd << ", Average: "  << avg_cwnd() << std::endl;


    update_cwnd(cwnd);

    // Return next quality
    int decision = congested ? 0 : qoe(safe_downscale * bandwidth, current_quality, VERBOSE).second;

    // update previous ts
    if (auto temp = client_.next_vts(); temp) { 
        previous_vts = temp.value();
    }

    curr_quality = decision;
    //temporary
    logQuality(std::to_string(client_.connection_id()), last_index, (double)decision, (double)cwnd);
    std::cerr << "Gap decision: " << decision << std::endl;
    if (VERBOSE) std::cerr << "--------------------------------------------" << std::endl;
    return vformats[decision];
}

double GapAbr::avg_cwnd() {
    double sum = 0.0;
    for (int i = 0; i < 5; i++) {
        sum += (double)prev_cwnd[i];
    }
    return sum / 5;
}

void GapAbr::update_cwnd(uint32_t cwnd) {
    for (int i = 0; i < 4; i++) {
        prev_cwnd[i] = prev_cwnd[i+1];
    }
    prev_cwnd[4] = cwnd;
}

GapAbr::GapAbr(const WebSocketClient & client, const std::string & abr_name, const YAML::Node & abr_config)
    : Target(client, abr_name, abr_config), prev_cwnd{0,0,0,0,0} {
    if (abr_config["verbose"]) {
        VERBOSE = abr_config["verbose"].as<bool>();
    }
}