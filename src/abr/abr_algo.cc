#include "abr_algo.hh"
#include <cmath>
#include <fstream>

using namespace std;

double ssim_db(const double ssim)
{
  if (ssim != 1) {
    return max(MIN_SSIM, min(MAX_SSIM, -10 * log10(1 - ssim)));
  } else {
    return MAX_SSIM;
  }
}

bool ABRAlgo::logQuality(const std::string username, const int ts, const double ssimdb) {
  std::string filename = "./abr/log/" + abr_name_ + "_" + username + ".txt";
  std::ofstream file;
  file.open(filename, std::ios::app);
  if (file.is_open()) {
    file << ts << "," << ssimdb << "," << std::endl;
    file.close();
    return true;
  } else {
    std::cerr << "Couldn't open file" << std::endl;
    return false;
  }
}

bool ABRAlgo::logQuality(const std::string username, const int ts, const double ssimdb, const double buffer) {
  std::string filename = "./abr/log/" + abr_name_ + "_" + username + ".txt";
  std::ofstream file;
  file.open(filename, std::ios::app);
  if (file.is_open()) {
    file << ts << "," << ssimdb << "," << buffer << std::endl;
    file.close();
    return true;
  } else {
    std::cerr << "Couldn't open file" << std::endl;
    return false;
  }
}