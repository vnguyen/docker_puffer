#include "target.hh"
#include "ws_client.hh"

#include <unordered_set>
#include <iostream>
#include <fstream>

namespace TargetConstants { 
  const int horizon = 10;  

  // constants used for QoE function weights
  const double alpha = 1.;
  const double beta = 2.5; 
  const double gamma = 100.; 
  const double zetta = 2. * gamma / horizon;

  // constants for bandwidth estimator
  const int bandwidth_window = 6;
  const int projection_window = 2;
  const int time_delta = 100;

  // constants for optimization objective
  const double qoe_percentile = .95;
  const double qoe_delta = .15;
  const int step = 1000;
  
  // constants for deciding quality
  const double safe_downscale = .8;
}

namespace {
  const int SECOND = 1000; 
}


namespace {
  struct state_t {
    state_t() {
      this->segment = -1;
      this->buffer = -1;
      this->quality = -1;
    }
  
    state_t(int segment, int buffer, int quality) {
      this->segment = segment;
      this->buffer = buffer;
      this->quality = quality;
    }

    int segment;
    int buffer;
    int quality;
  
    bool operator == (const state_t &other) const {
      return segment == other.segment && buffer == other.buffer;
    }

    bool operator != (const state_t &other) const {
      return !(*this == other);
    }

    void operator = (const state_t &other) { 
      this->segment = other.segment;
      this->buffer = other.buffer;
      this->quality = other.quality;
    }

    friend std::ostream& operator << (std::ostream &os, const state_t &value);
  };

  struct value_t {
    value_t() {
    }

    value_t(int qoe, int ssim, state_t from) {
      this->qoe = qoe;
      this->ssim = ssim;
      this->from = from;
    }

    int qoe; // [TODO] should this be double? 
    int ssim;
    state_t from; 

    void operator = (const value_t& other) { 
      this->qoe = other.qoe;
      this->ssim = other.ssim;
      this->from = other.from;
    }

    friend std::ostream& operator << (std::ostream &os, const value_t &value);
  };
 
  std::ostream& operator << (std::ostream &os, const state_t &value) { 
    return os << "state_t(segment: " << value.segment << ", quality: " 
              << value.quality << ", buffer: " << value.buffer << ")";
  }
    
  std::ostream& operator << (std::ostream &os, const value_t &value) { 
    return os << "value_t(qoe: " << value.qoe << ", ssim: " << value.ssim 
              << ", from: " << value.from << ")";
  }
}

double Target::localQoe(int current_ssim, int last_ssim, int rebuffer, int buffer) {
  return 1. * TargetConstants::alpha * current_ssim
    - 1. * TargetConstants::beta * fabs(current_ssim - last_ssim)
    - 1. * TargetConstants::gamma * rebuffer / ::SECOND;
}


std::pair<double, int> Target::qoe(const double bandwidth, int quality, bool verbose) {
  const auto & channel = client_.channel();
  const auto & vformats = channel->vformats();
  size_t vformats_cnt = vformats.size();

  uint64_t last_index = previous_vts; 
  uint64_t next_index = 0;
  double current_ssim = 0.0;
  double next_ssim = 0.0;
  size_t current_size = 0.0;

  unsigned int interval = channel->vduration();

  if (auto temp = client_.next_vts(); temp) { 
    next_index = temp.value();
  }

  if (client_.curr_vformat() && last_index != 0) {
    current_ssim = channel->vssim(client_.curr_vformat().value(), last_index);
    current_size = std::get<1>(channel->vdata(client_.curr_vformat().value(), last_index));
  }

  if (client_.curr_vformat() && next_index != 0) {
    next_ssim = channel->vssim(client_.curr_vformat().value(), next_index);
  }


  int start_index = last_index + interval;

  int start_buffer = (int)(client_.video_playback_buf() * 100);

  /*
  Data info:
  ssim range: [0, 1], but in reality [0.88,1]
  ssimdb range: in reality [8, 17]
  quality: [0,1..9]
  start_buffer range: [0, 1500], assuming max. buffer is 15s (setting in ws_client.hh)
  interval: segment ts length, should be constant per channel.
  size: kB
  bandwidth: kbps

  Note that the following console output refers to the previous cycle
  */

  if (verbose) {
    std::cerr << "Previous video send ts: " << last_index << std::endl
              << "Previous ssim: " << current_ssim << " SSIMdb: " << ssim_db(current_ssim) << std::endl
              << "Buffer: " << start_buffer << std::endl
              << "Next video send ts: " << next_index << std::endl
              << "Next ssim: " << next_ssim << " SSIMdb: " << ssim_db(next_ssim) << std::endl
              << "Segment duration: " << (double)channel->vduration() / channel->timescale() << std::endl
              << "Current size: " << current_size << std::endl
              << "Bandwidth (after safe downscale): " << bandwidth << std::endl;
  }

  /* temporary comment out
  logQuality(std::to_string(client_.connection_id()), last_index, ssim_db(current_ssim), (double)start_buffer);
  */
  // Let dp[s][b][q] be the total QoE for segment s with buffer occupancy b and quality q.
  // The s, b and q variables are contined in the `state_t` structure.

  // We use the forward recurrence:
  // dp[s + 1][b'][q'] = max(
  //     dp[s + 1][b'][q'] ,
  //     dp[s][b][q] + v(s + 1,q') + lambda |v(s, q) - v(s + 1,q')| + gamma r(s + 1,q')
  // )
  
  // The complexity is O(H∗B∗Q^2), where H is the length of the horizon, B is the number 
  // of buffer discrete levels and Q is the number of different qualities.
  std::function<size_t (const state_t &)> hash = [](const state_t& state) {
    size_t seed = 0;
    seed ^= std::hash<int>()(state.segment) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    seed ^= std::hash<int>()(state.buffer) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    seed ^= std::hash<int>()(state.quality) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    return seed;
  };
  std::unordered_map<state_t, value_t, std::function<size_t (const state_t&)> > dp(0, hash);
  std::unordered_set<state_t, std::function<size_t (const state_t&)> > curr_states(0, hash);
  
  // We use buffer units of 10ms, hence having a maximum number of discrete buffer levels 
  // of 1500 (i.e. 15 seconds).
  int buffer_unit = 10;
  int max_buffer = (15 * ::SECOND / buffer_unit) / 10;
  state_t null_state, start_state(last_index, start_buffer / buffer_unit, quality);
  dp[start_state] = value_t(0, current_ssim, null_state);
  curr_states.insert(start_state);
  
  int max_segment = 0;
  
  for (int current_index = last_index; current_index < start_index + TargetConstants::horizon * interval; current_index += interval) {
    
    std::unordered_set<state_t, std::function<size_t (const state_t&)> > next_states(0, hash);
    for (auto &from : curr_states) {
      
      int max_quality = std::min((int)(vformats_cnt - 1), from.quality + 1);
      int min_quality = std::max(0, from.quality - 2);
      
      for (int chunk_quality = min_quality; chunk_quality <= max_quality; ++chunk_quality) {
        double current_buffer = from.buffer * buffer_unit;
        double rebuffer = 0;
        
        double size_kb = 8. * std::get<1>(channel->vdata(vformats[chunk_quality], current_index + interval)) / ::SECOND;
        double download_time_ms = size_kb / bandwidth * ::SECOND;
        
        // simulate buffer changes
        if (current_buffer < download_time_ms) {
          rebuffer = download_time_ms - current_buffer;
          current_buffer = 0;
        } else {
          current_buffer -= download_time_ms;
        }

        // see channel class        
        int segment_length_ms = (int)(((double)channel->vduration() / channel->timescale()) * 100.0);
        current_buffer += segment_length_ms;
        current_buffer = std::min(current_buffer, 1. * max_buffer * buffer_unit);

        // compute next state
        state_t next(current_index + interval, current_buffer / buffer_unit, chunk_quality);

        // compute current and last ssim
        double current_ssimdb = ssim_db(next_ssim);
        int last_ssimdb = dp[from].ssim;
        
        // compute qoe
        double qoe = dp[from].qoe + localQoe((int)current_ssimdb, last_ssimdb, rebuffer, current_buffer);
        
        // update dp value with maximum qoe
        if (dp.find(next) == dp.end() || dp[next].qoe < qoe) {
          dp[next] = value_t(qoe, (int)current_ssimdb, from); 
          next_states.insert(next);

          max_segment = std::max(max_segment, (int)(current_index + interval));
        }
      }
    }
    curr_states = next_states;
  }
  
  
  // find best series of segments
  state_t best = null_state;
  for (int buffer = 0; buffer <= max_buffer; ++buffer) {
    for (int chunk_quality = 0; chunk_quality < (int)(vformats_cnt); ++chunk_quality) {
      state_t cand(max_segment, buffer, chunk_quality);
      if (dp.find(cand) != dp.end() && (best == null_state || dp[cand].qoe >= dp[best].qoe)) { 
        best = cand;
      }
    }
  }
 
  if (best == null_state) {
    if (verbose) std::cerr << "[Target] keeping quality" << std::endl; 
    return std::make_pair(0, quality);
  }

  // find first decision
  std::vector<state_t> states;
  state_t state = best;
  while (state != null_state) {
    states.push_back(state);
    state = dp[state].from;
  }
  std::reverse(states.begin(), states.end());
  state_t first = states.size() > 1 ? states[1] : states[0];
 
  if (verbose) {
      for (auto state : states) {
      std::cerr << state << std::endl;
    }
    std::cerr << "[Target] first: " << first << ' ' << dp[first] << std::endl;
    std::cerr << "[Target] best: " << best << ' ' << dp[best] << std::endl;
  }

  return std::make_pair(dp[best].qoe, first.quality);
}

VideoFormat Target::select_video_format() {
  if (VERBOSE) std::cerr << "---------------- Target ABR ----------------" << std::endl;
  const auto & channel = client_.channel();
  const auto & vformats = channel->vformats();
  size_t vformats_cnt = vformats.size();

  // handle std::optional
  uint64_t delivery_rate = 256; // can't be zero due to division
  if (client_.tcp_info()) {
    delivery_rate = client_.tcp_info().value().delivery_rate;
  }

  int bandwidth = 8 * delivery_rate / 100;
  /*

  This section will work "out of the box" when pacing gain cycles are implemented!
  However, this is only possible with QUIC

  // Get search range for bandwidth target
  int estimator = (int)bw_estimator->value_or(bandwidth);
  if (estimator < 0) {
    // overflow
    estimator = bandwidth;
  }

  int min_bw = int(fmin(estimator, bandwidth) * (1. - TargetConstants::qoe_delta));
  int max_bw = int(estimator * (1. + TargetConstants::qoe_delta));

  // Compute new bandwidth target for adjustCC -- this function should be strictly increasing 
  // as with extra bandwidth we can take the exact same choices as we had before
  

  std::cerr << "Looking for bandwidth_target (for CC): " << std::endl;
  bandwidth_target = max_bw;
  int qoe_max_bw = qoe(max_bw, curr_quality, false).first; 
  int step = TargetConstants::step;
  while (bandwidth_target - step >= min_bw && 
    qoe(bandwidth_target - step, curr_quality, false).first >= TargetConstants::qoe_percentile * qoe_max_bw
    ) {
    bandwidth_target -= step;
  }

  std::cerr << "[TargetAbr] bandwidth interval: [" << min_bw << ", " << max_bw << "]" << std::endl
            << "[TargetAbr] bandwidth current: " << bandwidth << std::endl
            << "[TargetAbr] bandwidth estimator: " << estimator << std::endl
            << "[TargetAbr] bandwidth target: " << bandwidth_target << std::endl;
  
  std::cerr << std::endl << "Deciding on next chunk quality: " << std::endl;
  */

  std::pair<double, int> decision = Target::qoe(TargetConstants::safe_downscale * bandwidth, curr_quality, VERBOSE);

  // update previous ts
  if (auto temp = client_.next_vts(); temp) { 
    previous_vts = temp.value();
  }

  // update quality
  curr_quality = std::get<1>(decision);

  if (VERBOSE) std::cerr << "--------------------------------------------" << std::endl;
  return vformats[std::get<1>(decision)];
}

/*
This is not working due to missing BBR configurability in TCP!
Refer to Alexandru Dan's code for pacing cycle interface.

void TargetAbr::adjustCC() {
  // Note we use adjusted level
  if (last_bandwidth == base::nullopt) { 
    return;
  }

  int bandwidth = last_bandwidth.value().value;
  if (bandwidth != last_adjustment_bandwidth) {
    double proportion = 1. * bandwidth / bandwidth_target;
    QUIC_LOG(WARNING) << "[TargetAbr] " << bandwidth << ' ' << proportion << '\n';
    if (proportion >= 1.3) {
      interface->proposePacingGainCycle(std::vector<float>{1, 0.8, 1, 0.8, 1, 1, 1, 1});
    } else if (proportion >= 0.9) {
      interface->proposePacingGainCycle(std::vector<float>{1.25, 0.75, 1, 1, 1, 1, 1, 1});
    } else if (proportion >= 0.5) {
      interface->proposePacingGainCycle(std::vector<float>{1.2, 1, 1.2, 1, 1, 1, 1, 1});
    } else {
      interface->proposePacingGainCycle(std::vector<float>{1.5, 1, 1.5, 1, 1, 1, 1, 1});
    }
    last_adjustment_bandwidth = bandwidth;
  }
}
*/

void Target::video_chunk_acked(Chunk && c) {
  if (VERBOSE) {
    std::cerr << "---------------- ACK ------------------" << std::endl
              << "Congestion window:  " << c.cwnd << std::endl
              << "RTT:                " << c.rtt << std::endl
              << "In flight:          " << c.in_flight << std:: endl
              << "---------------------------------------" << std::endl;
  }
}



Target::Target(const WebSocketClient & client, const std::string & abr_name, const YAML::Node & abr_config) 
    : ABRAlgo(client, abr_name), bw_estimator(new structs::LineFitEstimator<double>(
      TargetConstants::bandwidth_window,
      TargetConstants::time_delta,
      TargetConstants::projection_window
    ))
 {
   previous_vts = 0;
   curr_quality = 0;
   if (abr_config["verbose"]) {
     VERBOSE = abr_config["verbose"].as<bool>();
   }
}