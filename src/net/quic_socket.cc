#include <iostream>
#include <string>
#include <arpa/inet.h>

#include "socket.hh"
#include "quic_socket.hh"

QuicSocket::QuicSocket( FileDescriptor && fd ) : TCPSocket( std::move( fd ) ) {
	std::cerr << "Created QuicSocket!" << std::endl;

    initialize_addr();
}

char *QuicSocket::get_ip_text(quic::QuicSocketAddress &addr) const {
    char *ip_text = new char[INET_ADDRSTRLEN];
    const in_addr temp_inet_addr = addr.host().GetIPv4();
    inet_ntop(AF_INET, &temp_inet_addr, ip_text, INET_ADDRSTRLEN);

    return ip_text;
}

void QuicSocket::initialize_addr() {
    const sockaddr *peer_sockaddr = &Socket::peer_address().to_sockaddr();
    quic_peer_addr = quic::QuicSocketAddress(peer_sockaddr, Socket::peer_address().size());

    const sockaddr *local_sockaddr = &Socket::local_address().to_sockaddr();
    quic_local_addr = quic::QuicSocketAddress(local_sockaddr, Socket::local_address().size());

    std::cerr << "Quic Socket initialized: " << quic_peer_addr.host().IsInitialized() << std::endl
	<< "Quic peer port: " << quic_peer_addr.port() << std::endl
	<< "Quic peer IP: " << get_ip_text(quic_peer_addr) << std::endl
    << "Quic local port: " << quic_local_addr.port() << std::endl
    << "Quic local IP: " << get_ip_text(quic_local_addr) << std::endl;
}