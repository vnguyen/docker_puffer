#include "socket.hh"
#include "quic/core/quic_packet_writer.h"
#include "quic/platform/api/quic_socket_address.h"

class QuicSocket : public TCPSocket
{
    public:
    QuicSocket( FileDescriptor && fd );
    QuicSocket() : TCPSocket() {};
    
    quic::QuicSocketAddress quic_peer_addr;
    quic::QuicSocketAddress quic_local_addr;

    void initialize_addr();

    char *get_ip_text(quic::QuicSocketAddress &addr) const;
};
