# SETUP GUIDE

This setup guide is adapted from the [Puffer documentation](https://github.com/StanfordSNR/puffer/wiki/Documentation).

## System prerequisites
HTTP, HTTPS, 50000, 50001, (optional SSH) ports should be accessible.

## Set up docker
Install docker and create a new docker image running Ubuntu 18.04 LTS.

Launch a docker container. [The networking should be shared with the host network.](https://docs.docker.com/network/network-tutorial-host/)

## 
